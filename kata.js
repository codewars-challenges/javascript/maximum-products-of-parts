function maximumProductOfParts(number) {
    let combinations = get_combinations(number);
    const num_str = number.toString();
    let products = combinations.map(a =>
        [get_part(num_str, 0, a[0]), get_part(num_str, a[0], a[1]), get_part(num_str, a[0] + a[1], a[2])].reduce((prod, num) => prod * num, 1)
    );
    return Math.max(...products);
}

function get_combinations(number) {
    let num_size = number.toString().length;
    let combinations = [];
    for (let i = 1; i <= num_size - 2; i++) {
        for (let j = 1; j <= num_size - i - 1; j++) {
            let k = num_size - i - j;
            [i + "|" + j + "|" + k, i + "|" + k + "|" + j, j + "|" + i + "|" + k, j + "|" + k + "|" + i, k + "|" + i + "|" + j, k + "|" + j + "|" + i].forEach(combination => {
                if (!combinations.includes(combination)) combinations.push(combination);
            });
        }
    }
    return combinations.map(str => str.split("|").map(chr => parseInt(chr)));
}

function get_part(string, start, end) {
    return parseInt(string.substr(start, end));
}

//console.log(maximumProductOfParts(1234));
//console.log(maximumProductOfParts(4321));
//console.log(maximumProductOfParts(4224));
//console.log(maximumProductOfParts(5188680));
//console.log(maximumProductOfParts(8675309));